(function() {
    var open = false;
    var popup = document.getElementById("popup");
    var popupCallback = function() {
        popup.style.display = (open) ? "none" : "flex";
        open = !open;
    }

    document.getElementById("lm").onclick = popupCallback;
    document.getElementById("close_popup").onclick = popupCallback;
})();
