// Generate Games HTML
(function() {
    var games = document.getElementById('games_container')
    removeAllChildren(games);

    getJSON('data/game.min.json', function(json) {
        if (json) {
            var json_length = json.length
            for (var i = 0; i < json_length; i++) {
                var game = json[i];
                var ObjDOMGame = {
                    attr: [{name: 'className', value: 'games'}],
                    in: [
                        {
                            attr: [{name: 'className', value: 'cover'}],
                            in: [{
                                tag: 'img',
                                attr: [
                                    {name: 'src', value: game.cover},
                                    {name: 'alt', value: 'Antarka Games ' + game.name}
                                ]
                            }]
                        },
                        {
                            attr: [{name: 'className', value: 'other'}],
                            in: [
                                {
                                    tag: 'h2',
                                    text: game.name
                                },
                                {
                                    tag: 'div',
                                    attr: [{name: 'className', value: 'description'}],
                                    text: game.description
                                },
                                {
                                    attr: [{name: 'className', value: 'download'}],
                                    in: [{
                                        tag: 'a',
                                        attr: [
                                            {name: 'target', value: '_blank'},
                                            {name: 'href', value: game.link}
                                        ],
                                        text: game.label
                                    }]
                                }
                            ]
                        }
                    ]
                }

                nodeTreeGenerator([ObjDOMGame], games);
            }
        }
        else
            console.log('requete data/game.min.json échoué');
    });
})();

// Generate Team HTML
(function() {
    var team = $('#our_team .team');
    removeAllChildren(team);

    getJSON('data/team.min.json', function(json) {
        if (json) {
            var json_length = json.length;
            for (var i = 0; i < json_length; i++) {
                var member = json[i];

                var ObjDOMSocial = [];
                var social_length = member.social.length;
                for (var j = 0; j < social_length; j++) {
                    var social = member.social[j];

                    var elem = {
                        tag: 'a',
                        attr: [
                            {name: 'href', value: social.link},
                            {name: 'rel', value: 'nofollow'},
                            {name: 'target', value: '_blank'}
                        ],
                        in: [{
                            tag: 'img',
                            attr: [
                                {name: 'src', value: social.img},
                                {name: 'alt', value: member.pseudo + social.name}
                            ]
                        }]
                    };

                    ObjDOMSocial[j] = elem;
                }

                var ObjDOMMember = {
                    attr: [{name: 'className', value: 'member'}],
                    in: [
                        {
                            attr: [{name: 'className', value: 'avatar'}],
                            in: [{
                                tag: 'img',
                                attr: [
                                    {name: 'src', value: member.avatar},
                                    {name: 'alt', value: 'Antarka ' + member.pseudo}
                                ]
                            }]
                        },
                        {
                            attr: [{name: 'className', value: 'social'}],
                            in: ObjDOMSocial
                        },
                        {
                            attr: [{name: 'className', value: 'other'}],
                            in: [
                                {
                                    tag: 'strong',
                                    text: member.lastname + " " + member.firstname
                                },
                                {
                                    tag: 'span',
                                    attr: [{name: 'className', value: 'pseudo'}],
                                    text: member.pseudo
                                },
                                {
                                    tag: 'span',
                                    attr: [{name: 'className', value: 'role'}],
                                    text: member.role
                                }
                            ]
                        }
                    ]
                }

                nodeTreeGenerator([ObjDOMMember], team)
            }
        }
        else
            console.log('requete data/team.min.json échoué')
    });
})();

// fetch and parse blog
(function() {
    /**
    * Author : Purexo
    * simply handle xhr request with xml content
    * @param url : string, content url you want fetch
    * @param callback : function(result), your callback to handle result of your request
    * @param method : string, http method (GET POST PUT ...)
    * @param data : any, data you want give to the send (XMLHttpRequest.send) method
    */
    function xhr_xml(url, callback, method, data) {
        var xhr = new XMLHttpRequest();
        xhr.overrideMimeType('text/xml');
        xhr.open(method ? method : 'GET', url, true);
        xhr.onreadystatechange = function (aEvt) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    callback(xhr.responseXML);
                }
                else {
                    callback(null);
                }
            }
        }
        xhr.send(data);
    }

    var parser = new DOMParser();
    var blog = $('#our_blog');
    xhr_xml('https://blog.antarka.com/index.xml', function(xml) {
    //xhr_xml('data/text.xml', xml => {
        if (xml) {
            var items = xml.getElementsByTagName('item');
            var max_length = items.length > 5 ? 5 : items.length;
            for (var i = 0; i < max_length; i++) {
                var item = items[i];

                var title = item.getElementsByTagName('title')[0].firstChild.data;
                var description = item.getElementsByTagName('description')[0].firstChild;
                description = description && description.data || '';

                var link = item.getElementsByTagName('link')[0].firstChild.data;
                console.log([title, description, link]);

                var elems = nodeTreeGenerator([
                    {
                        attr: [{name: 'className', value: 'billet'}],
                        in: [
                            {
                                tag: 'a',
                                attr: [
                                    {name: 'href', value: link},
                                    {name: 'className', value: "title"},
                                    {name: 'taget', value: '_blank'}
                                ],
                                text: title
                            },
                            {
                                tag: 'hr'
                            },
                            {
                                tag: 'p',
                                attr: [{name: 'className', value: 'description'}]
                            }
                        ]
                    }
                ]);
                elems[0].querySelector('p.description').innerHTML = description;
                blog.appendChild(elems[0]);
            }
        }
        else {
            console.log('La requete a échoué')
        }
    })
})();
